// Simple
// d3.select("body")
//     .append("svg")
//         .attr("width", 50)
//         .attr("height", 50)
//         .append("circle")
//             .attr("cx", 25)
//             .attr("cy", 25)
//             .attr("r", 10)
//             .style("fill", "red");

// Better
// const body = d3.select("body");
//
// const svg = body.append("svg")
//     .attr("width", 50)
//     .attr("height", 50);
//
// const circle = svg.append("circle")
//     .attr("cx", 25)
//     .attr("cy", 25)
//     .attr("r", 10)
//     .style("fill", "red");
//
// svg.append("circle")
//     .attr("cx", 15)
//     .attr("cy", 15)
//     .attr("r", 5)
//     .style("fill", "blue");


const myData = [10,20,30];

d3.select("body")
    .selectAll("p")
    .data(myData)
    .enter()
    .append("p")
    //.text("hello");
    .text(function(d, i) { return "hello " + d + " " + i});


// domaine[totalMin..totalMax] -> range[couleurMin..couleurMax]
const colorScale = d3.scaleLinear
    .domain([totalMin, totalMax])
    .range(["#000000", "#FF0000"]);













