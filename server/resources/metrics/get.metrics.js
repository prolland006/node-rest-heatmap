/**
 * GET /metrics/users/:user_id/pages/:url
 *
 * Retourne les stats pour un couple (page | user) donné
 */
const metricsDb = require('../../db/metricsDb');

module.exports = function (req, res, next) {

    const user_id = req.params.user_id;
    const url = req.params.url; // reception url_encoded

    const params = {user_id, url};
    if(req.query.from) { params.from = parseInt(req.query.from) } // TODO handle errors

    metricsDb.getMetrics(params, function(err, data) {
        if(err) {
            console.log(err);
            return next(err);
        }

        res.send(data);
        return next();
    });

};