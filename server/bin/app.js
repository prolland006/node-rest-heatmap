const api = require('../api')();

api.listen(3000, function() {
    console.log('%s listening at %s', api.name, api.url);
});