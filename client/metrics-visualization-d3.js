const MAX_TOTAL_ALLOWED = 6;

const svg = d3.select("body").append("svg").attr("width", 400).attr("height", 400);

const metricsCache = [];
let timeStampSinceLastResult;

function pollMetrics() {
    let resourceURL = 'http://localhost:3000/metrics/users/user-123456789/pages/page-to-spy.html';

    if(timeStampSinceLastResult) {
        resourceURL += `?from=${timeStampSinceLastResult}`;
    }

    d3.json(resourceURL, function(data) {

        timeStampSinceLastResult = Date.now(); // TODO perte de données potentielle ici, aggréger le latestTimeStamp côté serveur

        updateMetricsCacheWithNewData(data);
        updateMetricsRepresentation();

    });

   setTimeout(pollMetrics, 1000); // poll for new data every 1s
}

pollMetrics();

function updateMetricsCacheWithNewData(newMetrics) {
    newMetrics.forEach(d => {

        let dataFoundInCache = metricsCache.find(cd => {
            return (cd.x === d.x) && (cd.y === d.y)
        });

        if(dataFoundInCache) {
            dataFoundInCache.total += d.total;
        } else {
            metricsCache.push(d);
        }
    });
}

function updateMetricsRepresentation() {
    // It might me better to work with sorted arrays http://stackoverflow.com/questions/11227809/why-is-it-faster-to-process-a-sorted-array-than-an-unsorted-array
    // const sortedData = data.sort((d1, d2) => { return d3.ascending(d1.total, d2.total) });
    const min = d3.min(metricsCache, function(d) { return d.total});
    const max =  d3.max(metricsCache, function(d) { return d.total});

    const colorScale = d3.scaleLinear()
        .domain([min, Math.min(max, MAX_TOTAL_ALLOWED)])
        .range(["#330000", "#FF0000"]);

    // D3 Selection https://github.com/d3/d3-selection
    // AND Data Joins see
    // https://github.com/d3/d3-selection#joining-data
    // https://bost.ocks.org/mike/join/
    // http://bl.ocks.org/mbostock/3808218
    // See here for key and objects consistency: https://bost.ocks.org/mike/constancy/
    const circleSelection = svg.selectAll("circle")
        .data(metricsCache, function(d) { return `${d.x}-${d.y}`});

    // EXIT, elements that have no corresponding data
    circleSelection.exit().remove(); // in case of database purge ?

    // UPDATE, elements that are already existing
    circleSelection
        .transition().duration(50) // transitions https://bost.ocks.org/mike/transition/
        .attr("r", function(d) { return 1 })
        .style("fill", function(d) { return colorScale(Math.min(d.total, MAX_TOTAL_ALLOWED))});

    // ENTER selection => create new elements for datum that has no corresponding element
    circleSelection
        .enter()
        .append("circle")
        .attr("cx", function(d) { return d.x })
        .attr("cy", function(d) { return d.y })
        .attr("r", function(d) { return 1 })
        .style("fill", function(d) { return colorScale(Math.min(d.total, MAX_TOTAL_ALLOWED))});

    // // MERGE selection: update + merge. Changed in D3 v4... see  https://medium.com/@mbostock/what-makes-software-good-943557f8a488#.d5c24uehp
    // utile pour mutualiser les operations sur UPDATE + ENTER
    // .merge(circleSelection)
    //     .attr("r", function(d) { return 5 })
    //     .style("fill", function(d) { return colorScale(Math.min(d.total, MAX_TOTAL_ALLOWED))});


    // We don't manage the elements visual order anymore.
    // In SVG it based on their declaration inside the <svg> elements.
    // We could use groups <g> for each different total value
    // or we just reorder the elements here.
    // But this should not be a problem if our circles have 1 pixel radius because they won't overlap...
    // const newCircleSelection = svg.selectAll("circle").sort(function(d1, d2) { return d3.ascending(d1.total, d2.total)});
}