let canvas = $("<canvas>")
    .attr("width", 400)
    .attr("height", 400);

$("body").prepend(canvas);

const metricsCache = [];
let timeStampSinceLastResult;

function pollMetrics() {
    let resourceURL = 'http://localhost:3000/metrics/users/user-123456789/pages/page-to-spy.html';

    if(timeStampSinceLastResult) {
        resourceURL += `?from=${timeStampSinceLastResult}`;
    }

    $.getJSON(resourceURL, function(data) {

        timeStampSinceLastResult = Date.now(); // TODO perte de données potentielle ici, aggréger le latestTimeStamp côté serveur

        updateMetricsCacheWithNewData(data);
        updateMetricsRepresentation();

    });

   setTimeout(pollMetrics, 1000); // poll for new data every 1s
}

pollMetrics();

function updateMetricsCacheWithNewData(newMetrics) {
    newMetrics.forEach(d => {

        let dataFoundInCache = metricsCache.find(cd => {
            return (cd.x === d.x) && (cd.y === d.y)
        });

        if(dataFoundInCache) {
            dataFoundInCache.total += d.total;
        } else {
            metricsCache.push(d);
        }
    });
}

function updateMetricsRepresentation() {
    let ctx = canvas[0].getContext('2d');
    const imageData = ctx.createImageData(400, 400);
    const pixels = imageData.data;

    for(let i = 0; i < metricsCache.length; i++) {
        const metric = metricsCache[i];

        if(metric.x > 400 || metric.y > 400) { continue; }

        imageData.data[4*(metric.y*imageData.width + metric.x)] = 255;
        imageData.data[4*(metric.y*imageData.width + metric.x) + 3] = 255;
    }

    ctx.putImageData(imageData, 0, 0);
}