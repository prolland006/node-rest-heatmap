function giveMeAColor(x, totalMin, totalMax) {
    const colorMin = 0;
    const colorMax = 255;

    y = ((x - totalMin) / (totalMax - totalMin)) * (colorMax - colorMin) + colorMin;

    return y;
}

$("body").data("test", 123456);

const MAX_TOTAL_ALLOWED = 6;
$.getJSON('http://localhost:3000/metrics/users/user-123456789/pages/page-to-spy.html', function(data) {

    const sortedData = data.sort((d1,d2) => { return d1.total - d2.total; });

    const min = sortedData[0].total;
    const max = Math.min(sortedData[sortedData.length - 1].total, MAX_TOTAL_ALLOWED);

    let svg = $(SVG_NS("svg"))
        .attr("width", 400)
        .attr("height", 400); // TODO prendre le min et le max des datas

    sortedData.forEach(d => {
        const r = Math.floor(giveMeAColor(Math.min(d.total, MAX_TOTAL_ALLOWED), min, max));

       let circle = $(SVG_NS("circle"))
                       .attr("cx", d.x)
                       .attr("cy", d.y)
                       .attr("r", Math.min(10, d.total))
                       .css("fill", `rgb(${r}, 0, 0)`);

       circle.data("d", d);

       circle.click(function(e) {
           // TODO essayer de remonter l'info via this
           console.log($.data(this, "d"));
           //console.log(circle.data("d")); // sinon closure mais on peut éviter de garder la reference
       });

       svg.append(circle);
   });

    $("body").prepend(svg);

});

function SVG_NS(e) {
    return document.createElementNS('http://www.w3.org/2000/svg', e);
}
//
// const circle = SVG(circle);
//
// $(SVG_NS(circle))
//
//
// document.createElement('svg')
// document.createElementNS('http://www.w3.org/2000/svg', 'svg');
//
// document.createElement('table')
// document.createElementNS('http://w3c.org', 'table');
// document.createElementNS('http://mobilier.com', 'table');

