const socket = io('http://localhost');

const METRICS_BUFFER_THRESHOLD = 10;
let metrics = [];

const MOUSE_IDLE_THRESHOLD = 5000;

let globalMouseIdleTimer;
let mousePositionX = null;
let mousePositionY = null;

function startMouseIdleTimer() {
    globalMouseIdleTimer = setInterval(() => {

        pushMetrics();

    }, MOUSE_IDLE_THRESHOLD);
}
function clearMouseIdleTimer() {
    clearInterval(globalMouseIdleTimer);
}
function pushMetrics() {
    if(mousePositionX == null && mousePositionY == null) {
        console.log("Can't push null mouse position", mousePositionX, mousePositionY);
        return;
    }

    const metric = {
        page : 'page-to-spy.html', // TODO grab the url directly from JavaScript
        user: 'user-123456789', // TODO sign in a user
        x: mousePositionX,
        y: mousePositionY,
        instant: Date.now()
    };

    console.log('Pushing metric', metric);

    metrics.push(metric);

    if(metrics.length === METRICS_BUFFER_THRESHOLD) {

        let metricsToSend = metrics.splice(0, METRICS_BUFFER_THRESHOLD);

        sendMetrics(metricsToSend);
    }
}
function sendMetrics(metricsToSend)  {
    socket.emit('send-metrics', metricsToSend);
}

startMouseIdleTimer();

$(window).mousemove(function(e) {

    clearMouseIdleTimer();

    mousePositionX = e.pageX;
    mousePositionY = e.pageY;

    console.log(mousePositionX, mousePositionY);
    pushMetrics();

    startMouseIdleTimer();

});